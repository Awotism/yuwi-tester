const Telegraf = require('telegraf');
const File_System = require('fs');
const My_Bot = new Telegraf(process.env.BOT_TOKEN);
const My_Token = '708877432';
const My_Token2 = '748543735';
const Alamat_Database_data = __dirname + '//' + 'database.txt';
const {markup} = require('telegraf');

//Waktu Saat Ini
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

var Time_Now;

function Waktu_Sekarang()
{
 var Time = new Date();

 Time_Now = '( ' + Time.getHours() + '' + Time.getMinutes() + ') ';
}

//Membaca Database
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

var Database_List = [1000000000], Database_Index = 0, Database_Total = 0;
var Read_Status = "Not Done";
var Read_Line = require('readline');
var Stream = require('stream');

var Mulai_Streaming = File_System.createReadStream(Alamat_Database_data);
var Selesai_Streaming = new Stream();
var Hasil_Stream = Read_Line.createInterface(Mulai_Streaming, Selesai_Streaming);

Hasil_Stream.on('line', function(Baris)
{
 if(Database_Index == 0)
 {
  Waktu_Sekarang();
  // console.log(Time_Now + 'Sedang membaca database ' + Database_Total + ' kode data.');
 }

 Database_List[Database_Index] = Baris;
 Database_Index += 1;
});

Hasil_Stream.on('close', function()
{
 Database_Total = Database_Index;
 Read_Status = "Done";

 Waktu_Sekarang();
 // console.log('\n' + Time_Now + 'Membaca database ' + Database_Total + ' kode data telah selesai.');
});

//Mengambil Kode data Dari Database
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

var Kode_data_Yang_Dipilih

function Mengambil_Kode_data()
{
 Waktu_Sekarang();
 // console.log('\n' + Time_Now + 'Bot sedang mengambil kode data dari database.');

 while(Database_Index < Parse_Indeks_Kode_data)
 {
  if(Database_Index == 0)
  Kode_data_Yang_Dipilih = Database_List[Database_Index];
  else if(Database_Index > 0)
  Kode_data_Yang_Dipilih += '\n' + Database_List[Database_Index];

  Database_Index += 1;
 }

 Waktu_Sekarang();
 // console.log('\n' + Time_Now + 'Bot selesai mengambil kode data dari database.');
}

//Cek File Penyimpanan Data Apakah Terdapat Dalam Local Disk
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

var Tanggal_Dan_Waktu, Parse_Tanggal_Dan_Waktu = [], Simpan_Dengan_Nama_File;

function Cek_File_Penyimpanan_Data()
{
 Database_Index = 0;
 Save_Status = "Not Done";
 Tanggal_Dan_Waktu = (new Date()).toString();
 Parse_Tanggal_Dan_Waktu = Tanggal_Dan_Waktu.split(" ");

 Simpan_Dengan_Nama_File = __dirname + '//' + 'data terjual' + '//' + 'order.txt';

 if(File_System.existsSync(Simpan_Dengan_Nama_File) == true)
 {
  Waktu_Sekarang();
  // console.log('\n' + Time_Now + 'Memulai untuk menyimpan kode data yang terjual pada lokasi \"' + Simpan_Dengan_Nama_File + '\".');

  Simpan_Data_data_Terjual();
 }
 else if(File_System.existsSync(Simpan_Dengan_Nama_File) == false)
 {
  Waktu_Sekarang();
  // console.log('\n' + Time_Now + 'Tidak dapat menemukan file \"' + Simpan_Dengan_Nama_File + '\" untuk menyimpan kode data yang terjual.');

  Waktu_Sekarang();
  // console.log('\n' + Time_Now + 'Sedang mencoba membuat file \"' + Simpan_Dengan_Nama_File + '\" baru untuk menyimpan kode data yang terjual.');

  File_System.writeFile(Simpan_Dengan_Nama_File, '', function (error)
  {
   if(error)
   {
    Waktu_Sekarang();
    // console.log('\n' + Time_Now + 'Membuat file \"' + Simpan_Dengan_Nama_File + '\" baru untuk menyimpan kode data yang terjual gagal dengan error :' + '\n\n' + error);
   }
   else
   {
	Waktu_Sekarang();
    // console.log('\n' + Time_Now + 'Membuat file \"' + Simpan_Dengan_Nama_File + '\" baru untuk menyimpan kode data yang terjual berhasil.');

	Simpan_Data_data_Terjual();
   }
  })
 }
}

//Menyimpan Data data Yang Telah Terjual
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

var Save_Status = "Done";

function Simpan_Data_data_Terjual()
{
 Waktu_Sekarang();
 // console.log('\n' + Time_Now + 'Sedang menyimpan file pada lokasi \"' + Simpan_Dengan_Nama_File + '\".');

 while(Database_Index < Parse_Indeks_Kode_data)
 {
  File_System.appendFile(Simpan_Dengan_Nama_File, '\n' + Database_List[Database_Index], function (error)
  {
   if(error)
 console.log(error);
  })

  Waktu_Sekarang();
  // console.log('\n' + Time_Now + 'Sedang menyimpan ' + (Database_Index + 1) + '/' + Parse_Indeks_Kode_data + ' kode data yang terjual.');

  Database_Index += 1;
 }

 Waktu_Sekarang();
 // console.log('\n' + Time_Now + 'Menyimpan file pada lokasi \"' + Simpan_Dengan_Nama_File + '\" berhasil.');

 Ubah_Database_data();
}

//Mengubah Isi Database Kode data
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

var Database_Value, Database_Reduce;

function Ubah_Database_data()
{
 Database_Value = "";
 Database_Reduce = Database_Total - Parse_Indeks_Kode_data;
 Database_Index = 0;

 Waktu_Sekarang();
 // console.log('\n' + Time_Now + 'Sedang mengubah file database pada lokasi \"' + Alamat_Database_data + '\".');

 while(Database_Index < Database_Reduce)
 {
  Database_List[Database_Index] = Database_List[Database_Index + Parse_Indeks_Kode_data];
  Database_Index += 1;
 }

 Database_Index = 0;

 while(Database_Index < Database_Reduce)
 {
  if(Database_Index == 0)
  Database_Value = Database_List[Database_Index];
  else if(Database_Index > 0)
  Database_Value += '\n' + Database_List[Database_Index];

  Waktu_Sekarang();
  // console.log('\n' + Time_Now + 'Sedang mengubah data ' + (Database_Index + 1) + '/' + Database_Reduce + ' pada database.');

  Database_Index += 1;
 }

 File_System.writeFile(Alamat_Database_data, Database_Value, function (error)
 {
  if(error)
   console.log(error);
 })

 Database_Index = 0;
 Save_Status = "Done";

 Waktu_Sekarang();
 // console.log('\n' + Time_Now + 'Mengubah file database pada lokasi \"' + Simpan_Dengan_Nama_File + '\" berhasil.');

 Waktu_Sekarang();
 // console.log('\n' + Time_Now + Parse_Indeks_Kode_data + ' kode data telah terjual, total database saat ini adalah ' + Database_Reduce +
//             ' yang sebelumnya adalah ' + Database_Total + '.');

 Database_Total = Database_Reduce;
}

//Deklarasi Tombol
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

const Tombol_Bantuan = Telegraf
                       .Extra
                       .markdown()
                       .markup( (m) => m.inlineKeyboard(
					   [
                         m.callbackButton('Cara Input', 'Tombol_Bantuan_On_Click')
                       ]))

//Tombol Apabila Ditekan
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

My_Bot.action('Tombol_Bantuan_On_Click', (ctx) =>
{
 ctx.reply('ketik \"data n\" untuk menampilkan kode data yang terjual dari indeks 0 sampai dengan indeks \"n\" dari database.')

 Waktu_Sekarang();
 // console.log('\n' + Time_Now + 'User [ ' + Username + ' ] menekan tombol [ Cara Input ].');
})

//Pembukaan
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

var Authentication, Username, Argumen = [], Indeks_Kode_data, Parse_Indeks_Kode_data, Kode_data_Database;

My_Bot.start( (ctx) =>
{
 Authentication = ctx.from.id;
 Username = ctx.message.from.username;

 if(Authentication == My_Token || Authentication == My_Token2)
 {
  ctx.reply('Apa ada yang dapat saya bantu, Master ' + Username + ' ?')
  .then(() => ctx.reply('Opsi bantuan :', Tombol_Bantuan))
 }
 else if(Authentication != My_Token)
 {
  ctx.reply('Apa ada yang dapat saya bantu, tuan ' + Username + ' ?')
  .then(() => ctx.reply('Opsi bantuan :', Tombol_Bantuan))
 }
})

//Membalas Pesan Tertentu
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

My_Bot.hears('bantuan', (ctx) =>
{
 Authentication = ctx.from.id;
 Username = ctx.message.from.username;

 ctx.reply('Opsi bantuan :', Tombol_Bantuan);
})

My_Bot.hears('copas', (ctx) =>
{
 ctx.reply('```Mantap```');
})

var Message_Index, Message_Total, Message_Group_Index, Message_Group_Total;

My_Bot.hears('show keyboard', (ctx) =>
{
const message = `woke master`;
ctx.telegram.sendMessage(ctx.message.chat.id, message, {
  parse_mode: 'Markdown',
  reply_markup: JSON.stringify({ // You also missed JSON.stringify()
    keyboard: [
        ['data 10' , 'data 20'],['data 50' ,
        'data 100']
    ],
    resize_keyboard: true
  }),
  disable_notification: false
});
})

//Membalas Pesan Tidak Tentu
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

var Pesan_Diterima;

My_Bot.on('text', (ctx) =>
{
 Pesan_Diterima = ctx.message.text;
 Authentication = ctx.from.id;
 Username = ctx.message.from.username;
 Argumen = Pesan_Diterima.split(" ");

 Waktu_Sekarang();
 // console.log('\n' + Time_Now + 'Bot mendapat pesan ' + '\"' + Pesan_Diterima + '\"' + ' dari user [ ' + Username + ' ].');

 if(Pesan_Diterima.indexOf("data") != -1)
 {
  if(Authentication == My_Token || Authentication == My_Token2)
  {
   if(Read_Status == "Done" && Save_Status == "Done")
   {
	if(Argumen.length == 1)
    ctx.reply('Indeks kode datanya tidak boleh kosong, Master ' + Username + '.')
    else if(Argumen.length == 2)
    {
	 Indeks_Kode_data = Argumen[1];
	 Parse_Indeks_Kode_data = parseInt(Indeks_Kode_data);

	 if(isNaN(Indeks_Kode_data) == true)
     ctx.reply('Indeks kode datanya harus berupa nomor atau angka, Master ' + Username + '.')
     else if(isNaN(Indeks_Kode_data) == false)
     {
      if(Parse_Indeks_Kode_data > Database_Total)
 	  ctx.reply('Indeks kode datanya melebihi total data pada database, Master ' + Username + '.');
      else if(Parse_Indeks_Kode_data <= Database_Total)
 	  {
       Database_Index = 0;

       ctx.reply('Wait..')
  //     .then(() => ctx.reply('Saya akan mencarikan terlebih dahulu kode datanya dari database indeks 1 sampai dengan indeks ' + Indeks_Kode_data + '.'))
	   /* Mengirim Semua data
	   .then(Mengambil_Kode_data())
	   .then(() => ctx.reply('Kode data yang terjual dari database indeks 1 sampai dengan indeks ' + Indeks_Kode_data + ' yaitu :' + '\n\n' + Kode_data_Yang_Dipilih))
	   */
	   /* Mengirim 1 data Sampai Sisa data Terakhir
	   .then
	   (
	    function Reply_1_By_1()
		{
         Waktu_Sekarang();
         // console.log('\n' + Time_Now + 'Bot sedang mengambil kode data dari database.');

	     Message_Index = 0;
         Message_Total = Parse_Indeks_Kode_data;

         while(Message_Index < Message_Total)
         {
		  ctx.reply('`' + Database_List[Message_Index] + '`');
		  Message_Index += 1;
         }

         Waktu_Sekarang();
         // console.log('\n' + Time_Now + 'Bot selesai mengambil kode data dari database.');
        }
	   )
	   */
	   // Mengirim 50 data Sampai Sisa data Terakhir
	   .then
	   (
	    function Reply_50()
		{
         Waktu_Sekarang();
         // console.log('\n' + Time_Now + 'Bot sedang mengambil kode data dari database.');

		 Message_Group_Index = 1;
		 Database_Index = 0;
	     Message_Index = 0;
         Message_Total = 50; //Untuk Merubah Banyaknya Pesan Sekali Pengiriman
		 Message_Group_Total = 0;

		 while(Message_Index < Parse_Indeks_Kode_data)
		 {
          if(Database_Index < Message_Total)
          {
           if(Database_Index == 0)
           Kode_data_Yang_Dipilih = Database_List[Message_Index];
           else if(Database_Index > 0)
           Kode_data_Yang_Dipilih += '\n' + Database_List[Message_Index];

           Message_Index += 1;
           Database_Index += 1;
		   Message_Group_Total += 1;
		  }
          else if(Database_Index == Message_Total)
          {
           ctx.reply(Kode_data_Yang_Dipilih)

		   Message_Group_Index += Message_Total;
           Database_Index = 0;
		  }
         }

         ctx.reply(Kode_data_Yang_Dipilih)
         .then(() => ctx.reply('Sudah master! 😘\nSisa data : ' + Database_Reduce + ' Data\nhttps://t.me/c/1289603773/458'))
          Waktu_Sekarang();
         // console.log('\n' + Time_Now + 'Bot selesai mengambil kode data dari database.');
        }
	   )
	   .then(() => Cek_File_Penyimpanan_Data())
      }
	 }
    }
	else if(Argumen.length >= 3)
    ctx.reply('Input berlebihan dan tidak sesuai dengan prosedur, Master ' + Username + '.')
   }
   else if(Read_Status == "Not Done")
   ctx.reply('Membaca database kode datanya masih belum selesai, Master ' + Username + '.');
   else if(Save_Status == "Not Done")
   ctx.reply('Menyimpan database kode datanya masih belum selesai, Master ' + Username + '.');
  }
  else if(Authentication != My_Token)
  ctx.reply('Maaf, hanya Master saya yang dapat mengakses kode data. Kalau mau beli data silahkan chat ke master saya\n @ff0x00');
 }
 else
 {
  if(Authentication == My_Token || Authentication == My_Token2)
  {
   ctx.reply('Pertanyaan tidak dikenal, Master ' + Username + '.')
   .then(() => ctx.reply('Atau ketik \"bantuan\" untuk menampilkan opsi bantuan, Master ' + Username + '.'))
  }
  else if(Authentication != My_Token)
  {
   ctx.reply('Pertanyaan tidak dikenal tuan ' + Username + '.')
   .then(() => ctx.reply('Atau ketik \"bantuan\" untuk menampilkan opsi bantuan, tuan ' + Username + ' ?'))
  }
 }
})

My_Bot.startPolling();
